* Сделал плюсовую версию функции **generateMultinodeRoute**, старая сишная использует плюсовую версию

* Перед рефакторингом нужно обязательно покрыть эту функцию тестами, чтобы потом проверить на них новый вариант.

* **generateMultinodeRoute** разбил на три функции: **remove_close_nodes**, **make_spline**, **make_exact_route**

* Не стал экономить на мелочах и решил, что эти функции могут возвращать вектор копированием

* Для **BSpline** и **CRSpline** добавил _const_ к параметрам

* убрал **#pragma once**

* Так как **length** и **dConvexLength** не меньше нуля, то
```c++
if((int)(length  /  (0.7 * dConvexLength )))
```
  равносильно
```c++
if (length >= 0.7 * dConvexLength)
```

* мы знаем, что **cNode > 0**, значит:
```c++
int nSpline = cNode<2 ? 1 : ( cNode<3? 2 : 3 );
```
равносильно
```c++
int nSpline = std::min(cNode, 3);
```

* Похоже, автор оригинального кода забыл, что есть **NxVec3::distance**

* Округление:
```c++
  int n_seg     = (int)ratio;
    if( (ratio - (int)ratio) >= 0.5)
      n_seg    =  (int)ratio + 1;
```
равносильно:
```c++
 int n_seg = std::lround(ratio);
```

* Смутил алгоритм удаления близких точек, я бы сравнивал не с предыдущей, а с последней из добавленных, то есть length вычислял бы так:
```c++
 in_node[idx].distance(result.back());
```

* В make_spline осталась каша с вычислением длины сплайна

* В целом кол-во строчек кода возрасло, но это связано с разбиением на функции, добавлением c++ версии и улучшением читаемости

* Задал тип для **enum** и использовал его

* Не использовал для **switch-case** вариант **default**, чтобы дать компилятору возможность выдавать предупреждения

* Добавил функцию **get_spline_type**, чтобы код оставался как и прежде рабочим для любых входных значениях **nSplineType**, так как по коду следует, что выходящие за пределы значения не являются ошибкой. Для нового кода я бы расставил ассерты, чтобы пользователи были более внимательными.
