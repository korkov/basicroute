#include "stdafx.h"
#include "BasicRoute.h"

#include "NxCommon.h"
#include <assert.h>
#include <cmath>
#include <vector>
#include <algorithm>

namespace
{
  std::vector<NxVec3> remove_close_nodes(const std::vector<NxVec3> in_node,
                                         double convex_length)
  {
    std::vector<NxVec3> result;
    result.push_back(in_node[0]);

    for (size_t idx = 1; idx < in_node.size(); idx++)
    {
      const NxReal length = in_node[idx].distance(in_node[idx - 1]);

      const NxReal magic_coefficient = 0.7;
      if (length >= magic_coefficient * convex_length)
        result.push_back(in_node[idx]);
    }
    return result;
  }


  NxReal route_length(const std::vector<NxVec3>& node)
  {
    NxReal sum = 0;
    for (size_t idx = 1; idx < node.size(); idx++)
      sum += node[idx].distance(node[idx - 1]);
    return sum;
  }

  spline_enum get_spline_type(int type)
  {
    switch(type)
    {
    case NO_SPLINE:
    case BICUBIC_SPLINE:
    case CR_SPLINE:
      return (spline_enum)type;
    }
    return NO_SPLINE;
  }

  void calc_spline(const std::vector<NxVec3>& node,
                   std::vector<NxVec3>& spline_node,
                   spline_enum type)
  {
    switch(type)
    {
    case BICUBIC_SPLINE:  
    {
      const size_t max_degree = 3;
      const size_t degree = std::min(node.size(), max_degree);
      BSpline spline;
      spline.bspline(node.size() - 1, degree, node.data(),
                     spline_node.data(), spline_node.size());
      spline_node.front() = node.front();
      spline_node.back() = node.back();
      break;
    }
    case CR_SPLINE: 
    {
      CRSpline spline;
      spline.crspline(node.data(), node.size(),
                      spline_node.data(), spline_node.size());
      break;
    }
    case NO_SPLINE:
    {
      spline_node.resize(std::min(node.size(), spline_node.size()));
      break;
    }
    }
  }


  std::vector<NxVec3> make_spline(const std::vector<NxVec3>& node,
                                  spline_enum type,
                                  NxReal convex_length,
                                  bool more_smooth)
  {
    const NxReal length = route_length(node);

    const bool smooth = more_smooth && (length > convex_length);
    const size_t spline_size =
      smooth ? length/convex_length : node.size() * 2;

    std::vector<NxVec3> spline_node = node;
    spline_node.resize(spline_size);

    calc_spline(node, spline_node, type);

    return spline_node;
  }


  std::vector<NxVec3> make_exact_route(const std::vector<NxVec3>& node,
                                       const std::vector<NxVec3>& spline_node,
                                       size_t max_size,
                                       NxReal convex_length)
  {
    std::vector<NxVec3> out_node;
    out_node.push_back(spline_node[0]);

    for (size_t idx = 1; idx < spline_node.size(); idx++)
    {
      NxVec3 delta = spline_node[idx] - out_node.back();
      const NxReal ratio = delta.magnitude() / convex_length;
      const long n_seg = std::lround(ratio);

      if (n_seg >= 1)
      {
        delta.normalize();
        for (size_t i = 0; i < n_seg; i++)
        {
          const NxVec3 point = out_node.back()  +  delta * convex_length;
          if (out_node.size() < max_size)
            out_node.push_back(point);
          else
            out_node.back() = point;
        }
      }
    }
    out_node.front() = node.front(); 
    out_node.back() = node.back();

    assert(out_node.size() <= max_size);

    return out_node;
  }
}


std::vector<NxVec3> generateMultinodeRoute(const std::vector<NxVec3>& in_node,
                                           double convex_length,
                                           size_t max_size,
                                           spline_enum spline_type,
                                           bool more_smooth)
{
  assert(!in_node.empty());
  assert(in_node.size() > 1);
  assert(convex_length > 0);

  const std::vector<NxVec3> node =
    remove_close_nodes(in_node, convex_length);

  const std::vector<NxVec3> spline_node =
    make_spline(node, spline_type, convex_length, more_smooth);

  return make_exact_route(node, spline_node, max_size, convex_length);
}


int generateMultinodeRoute(NxVec3* in_node, int cNode,
                           double dConvexLength,
                           NxVec3* out_node, int cMaxOut,
                           int nSplineType,
                           bool bMoreSmooth)
{
  assert(in_node);
  assert(cNode > 1);
  assert(dConvexLength > 0);

  const std::vector<NxVec3> node(in_node, in_node + cNode);

  const spline_enum type = get_spline_type(nSplineType);

  const std::vector<NxVec3> result =
    generateMultinodeRoute(node, dConvexLength, cMaxOut, type, bMoreSmooth);

  std::copy(result.begin(), result.end(), out_node);
  return result.size();
}
