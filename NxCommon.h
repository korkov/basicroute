#pragma once
#include <vector>
#include "BasicRoute.h"

//B - Spline
class BSpline
{
public:

  // Constructors and destructor
  BSpline();
  ~ BSpline();

  bool      bDump;
  void      bspline(int n, int t, const NxVec3 *control, NxVec3 *output, int num_output);

  // Operations
  void       compute_intervals(int *u, int n, int t);
  NxReal     blend(int k, int t, int *u, NxReal v);
  void       compute_point(int *u, int n, int t, NxReal v, const NxVec3 *control, NxVec3 *output);

};

//------------------------------------------------------------------
//Catmull-Rom  Spline
class CRSpline
{
public:

  // Constructors and destructor
  CRSpline();
  ~CRSpline();

  bool   bDump;
  void   crspline(const NxVec3 *control, int num_control, NxVec3 *output, int num_output);


  // Operations
  void      addSplinePoint(const NxVec3& v);
  NxVec3    getInterpolatedSplinePoint(float t);   // t = 0...1; 0=vp[0] ... 1=vp[max]
  int       getNumPoints();
  NxVec3&   getNthPoint(int n);


  // Static method for computing the Catmull-Rom parametric equation
  // given a time (t) and a vector quadruple (p1,p2,p3,p4).
  static NxVec3 Eq(float t, const NxVec3& p1, const NxVec3& p2, const NxVec3& p3, const NxVec3& p4);

private:
  std::vector<NxVec3> vp;
  float delta_t;
};
