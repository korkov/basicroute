#include "NxCommon.h"

/*********************************************************************
// b-spline curve algorithm
**********************************************************************/

BSpline::BSpline()
{
  bDump = false;
}
//*===================================================================================================================================================;
BSpline::~BSpline()
{
}

void BSpline::bspline(int n, int t, const NxVec3 *control, NxVec3 *output, int num_output)

/*********************************************************************

Parameters:
n          - the number of control points minus 1
t          - the degree of the polynomial plus 1
control    - control point array made up of point stucture
output     - array in which the calculate spline points are to be put
num_output - how many points on the spline are to be calculated

Pre-conditions:
n+2>t  (no curve results if n+2<=t)
control array contains the number of points specified by n
output array is the proper size to hold num_output point structures


**********************************************************************/

{

  FILE  *spline = NULL;

  if(!spline &&  bDump ) 
    spline = fopen ("BSpline.txt", "w");

  int *u;
  NxReal increment,interval;
  NxVec3 calcxyz;
  int output_index;

  u = new int[n + t + 1];

  compute_intervals(u, n, t);

  increment = (NxReal) (n - t + 2) /(  num_output - 1 );  // how much parameter goes up each time
  interval=0;

  if(spline)
  {
    for (output_index=0; output_index<= n; output_index++)
    {
      fprintf(spline,"%5.2f,%5.2f,%5.2f\n",control[output_index].x,control[output_index].y,control[output_index].z );
    }
    fprintf(spline,"\n");
  }

  for (output_index=0; output_index<num_output-1; output_index++)
  {
    compute_point(u, n, t, interval, control, &calcxyz);
    output[output_index].x = calcxyz.x;
    output[output_index].y = calcxyz.y;
    output[output_index].z = calcxyz.z;
    interval=interval+increment;  // increment our parameter

    if(spline)
      fprintf(spline,"%5.2f,%5.2f,%5.2f\n",output[output_index].x,output[output_index].y,output[output_index].z );

  }
  output[num_output-1].x=control[n].x;   // put in the last point
  output[num_output-1].y=control[n].y;
  output[num_output-1].z=control[n].z;

  if(spline)
    fprintf(spline,"%5.2f,%5.2f,%5.2f\n",output[num_output-1].x,output[num_output-1].y,output[num_output-1].z );

  if(spline)
    fclose(spline);
  delete [] u;
}
//*==========================================================================;
NxReal BSpline::blend(int k, int t, int *u, NxReal v)  // calculate the blending value
{
  NxReal value;

  if (t==1)			// base case for the recursion
  {
    if ((u[k]<=v) && (v<u[k+1]))
      value=1;
    else
      value=0;
  }
  else
  {
    if ((u[k+t-1]==u[k]) && (u[k+t]==u[k+1]))  // check for divide by zero
      value = 0;
    else
      if (u[k+t-1]==u[k]) // if a term's denominator is zero,use just the other
        value = (u[k+t] - v) / (u[k+t] - u[k+1]) * blend(k+1, t-1, u, v);
      else
        if (u[k+t]==u[k+1])
          value = (v - u[k]) / (u[k+t-1] - u[k]) * blend(k, t-1, u, v);
        else
          value = (v - u[k]) / (u[k+t-1] - u[k]) * blend(k, t-1, u, v) +
          (u[k+t] - v) / (u[k+t] - u[k+1]) * blend(k+1, t-1, u, v);
  }
  return value;
}
//*==========================================================================;
void BSpline::compute_intervals(int *u, int n, int t)   // figure out the knots
{
  int j;

  for (j  = 0; j  <=  n  +  t; j++)
  {
    if (j < t)
    {
      u[j]=0;
    }
    else
    {
      if ((t <= j)  &&  (j <=n ))
      {
        u[j]  = j - t + 1;
      }
      else
      {
        if (j>n)
        {
          u[j]=n-t+2;
        }
      }
    }
  }
}
//*==========================================================================;
void BSpline::compute_point(int *u, int n, int t, NxReal v, const NxVec3 *control,
                            NxVec3 *output)
{
  int k;
  NxReal temp;

  // initialize the variables that will hold our outputted point
  output->x=0;
  output->y=0;
  output->z=0;


  for (k = 0;  k <= n;  k++)
  {
    temp = blend(k,t,u,v);  // same blend is used for each dimension coordinate

    output->x = output->x + (control[k]).x * temp;
    output->y = output->y + (control[k]).y * temp;
    output->z = output->z + (control[k]).z * temp;

  }
}
//*===================================================================================================================================================;
//RS-spline curve algorithm
//*===================================================================================================================================================;
CRSpline::CRSpline(): vp(), delta_t(0)
{
  bDump = false;
}
//*===================================================================================================================================================;
CRSpline::~CRSpline()
{
  vp.clear();
}
//*===================================================================================================================================================;
// Solve the Catmull-Rom parametric equation for a given time(t) and vector quadruple (p1,p2,p3,p4)
NxVec3 CRSpline::Eq(float t, const NxVec3& p1, const NxVec3& p2, const NxVec3& p3, const NxVec3& p4)
{
  float t2 = t * t;
  float t3 = t2 * t;

  float b1 = .5 * (  -t3 + 2*t2 - t);
  float b2 = .5 * ( 3*t3 - 5*t2 + 2);
  float b3 = .5 * (-3*t3 + 4*t2 + t);
  float b4 = .5 * (   t3 -   t2    );

  return (p1*b1 + p2*b2 + p3*b3 + p4*b4); 
}
//*===================================================================================================================================================;
void CRSpline::addSplinePoint(const NxVec3& v)
{
  vp.push_back(v);
  delta_t = (float)1 / (float)vp.size();
}
//*===================================================================================================================================================;
NxVec3 CRSpline::getInterpolatedSplinePoint(float t)
{
  // Find out in which interval we are on the spline
  int p = (int)(t / delta_t);
  // Compute local control point indices
#define BOUNDS(pp) { if (pp < 0) pp = 0; else if (pp >= (int)vp.size()-1) pp = vp.size() - 1; }

  int p0 = p - 1;     BOUNDS(p0);
  int p1 = p;         BOUNDS(p1);
  int p2 = p + 1;     BOUNDS(p2);
  int p3 = p + 2;     BOUNDS(p3);

  // Relative (local) time 
  float lt = (t - delta_t*(float)p) / delta_t;
  // Interpolate
  return CRSpline::Eq(lt, vp[p0], vp[p1], vp[p2], vp[p3]);
}
//*===================================================================================================================================================;
int CRSpline::getNumPoints()
{
  return vp.size();
}
//*===================================================================================================================================================;
NxVec3& CRSpline::getNthPoint(int n)
{
  return vp[n];
}
//*===================================================================================================================================================;
void CRSpline::crspline(const NxVec3 *control, int num_control, NxVec3 *output, int num_output)
{
  FILE  *spline = NULL;

  if(!spline &&  bDump ) 
    spline = fopen ("CRpline.txt", "w");


  for (int i = 0; i < num_control; i++)
  {
    addSplinePoint(control[i]);

    if(spline)
      fprintf(spline,"%5.2f,%5.2f,%5.2f\n",control[i].x,control[i].y,control[i].z );
  }

  for (int i = 0; i < num_output; i++) 
  {
    float t = (float)i / (float)num_output;
    output[i] = getInterpolatedSplinePoint(t);

    if(spline)
      fprintf(spline,"%5.2f,%5.2f,%5.2f\n",output[i].x,output[i].y,output[i].z );

  }

  if(spline)
    fclose(spline);

}
