#ifndef BASIC_ROUTE_H___
#define BASIC_ROUTE_H___

#include <vector>

enum spline_enum
{
  NO_SPLINE = 0,
  BICUBIC_SPLINE = 1,
  CR_SPLINE = 2
};

int generateMultinodeRoute(NxVec3* in_node, int cNode,
                           double dConvexLength,
                           NxVec3 * out_node, int nMaxOut,
                           int SplineType = BICUBIC_SPLINE,
                           bool bMoreSmooth = false);

std::vector<NxVec3> generateMultinodeRoute(const std::vector<NxVec3>& in_node,
                                           double convex_length,
                                           size_t max_size,
                                           int spline_type = BICUBIC_SPLINE,
                                           bool more_smooth = false);

#endif // BASIC_ROUTE_H___
